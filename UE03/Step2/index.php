<?php
/**
 * Created by PhpStorm.
 * User: brunner
 * Date: 25.01.2021
 * Time: 10:12
 */

try {
    $pdo = new PDO("mysql:host=localhost;dbname=trackstar", "root", "");
} catch (PDOException $e) {
    die($e->getMessage());
    //die('<p>System not available!</p>');
}
$statusMsg = '';
// Step 2 Task 2.2
if ($_GET && isset($_GET['delete_project'])) {
    $stmt = $pdo->prepare("DELETE FROM projects WHERE project_id = :project_id");
    $stmt->bindParam(':project_id', $_GET['delete_project'], PDO::PARAM_INT);
    $stmt->execute();
    // check if "delete" was successful
    //Ternary operator == if...else
    $statusMsg = ($stmt->rowCount() == 1) ? '<p><div class="alert alert-primary" role="alert">Project (id=' . $_GET["delete_project"] . ') deleted</div></p>' : '<p><div class="alert alert-danger" role="alert">Project could not be deleted</div></p>';
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link href="screen.css" rel="stylesheet">
    <title>Trackstar</title>
</head>
<body>
<div class="container">
    <header class="position-relative">
        <div class="position-absolute top-0 end-0 mt-4"><span>User:</span> Brunner</div>
    </header>
    <main>
        <h1>Projektüberischt</h1>
        <?php echo $statusMsg; ?>
        <table class="table table-striped project-table">
            <thead>
            <tr>
                <th scope="col">Projektname</th>
                <th scope="col">Beschreibung</th>
                <th scope="col">Datum</th>
                <th scope="col">Operationen</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $projects = $pdo->query("SELECT * FROM projects")->fetchAll();
            foreach ($projects as $project) { ?>
                <tr>
                    <td><?php echo $project['title'] ?></td>
                    <td><?php echo $project['description'] ?></td>
                    <td><?php echo $project['created_at'] ?></td>
                    <td>
                        <i class="bi bi-trash"></i>
                        <i class="bi bi-pencil"></i></a>
                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?show_project=' . $project['project_id'] ?>"><i
                                    class="bi bi-card-list"></i></a>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <br>
        <?php
        // Step 2 Task 2.1
        if ($_GET && isset($_GET['show_project'])) {
            $p_id = $_GET['show_project'];
            $data = $pdo->query("select p.project_id, p.title, i.description, i.category, i.created_at from projects p join issues i on p.project_id=i.project_id where p.project_id=$p_id")->fetchAll();
            //print_r($data);
            $p_title = "";
            if (!$data)
                //No issues available, we need to load the project title!
                $p_title = $pdo->query("SELECT title FROM projects WHERE project_id = $p_id")->fetchColumn();
            else
                $p_title = $data[0]["title"];
            ?>
            <h3>Issue List <?php echo $p_title; ?></h3>
            <p>
                <a class="btn btn-primary"
                   href="<?php echo $_SERVER['PHP_SELF'] . '?new_issue_for_project=' . $p_id ?>">New Issue</a>
            </p>
            <?php if ($data != false) { ?>
                <table class="table table-striped project-table">
                    <tbody>
                    <?php
                    foreach ($data as $issue) { ?>
                        <tr>
                            <td><?php echo $issue['description'] ?></td>
                            <td><?php echo $issue['category'] ?></td>
                            <td><?php echo $issue['created_at'] ?></td>
                        </tr>
                        <?php
                    } ?>
                    </tbody>
                </table>
                <?php
            } else {
                echo "<p>Keine Issues verfügbar!</p>";
            }
        }
        ?>
    </main>
</div>
<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
        crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
-->
</body>
</html>