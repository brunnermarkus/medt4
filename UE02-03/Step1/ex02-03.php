<!--
Step 1:
 + Rote Stern weist auf einPflichtfeld hin
 + Serverseitige Auswertung, ob alle Pflichtfelder vorhanden sind
 + Hinweis: Statt GET sollte man POST verwenden - just 4 demo!
-->
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Übung 02-03, Step 1 - Fomrularverarbeitung mit PHP</title>
    <style>
        form .mandatory:after{
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<?php
$formData = [
    "salutation",
    "title" ,
    "firstname",
    "lastname",
    "city",
    "street",
    "zip",
    "mail" ,
    "state",
    "interests"]
?>

<div class="container">
    <div class="row">
        <div class="col mt-3">
            <h1 class="bg-light">Kontaktformular</h1>
            <?php if (isset($_GET['sBtn'])) { ?>
                <div class="alert alert-secondary" role="alert">
                    <h5>Sie haben folgende Eingaben getätigt:</h5>
                    <ul>
                        <?php
                        foreach ($formData as $formItem) {
                            $value = $_GET[$formItem];
                            if (is_array($_GET[$formItem])) {
                                $value="";
                                foreach ($_GET[$formItem] as $item)
                                    $value .= $item." ";
                            }
                            echo "<li>{$value}</li>";
                        }
                        ?>
                    </ul>
                </div>
            <?php } ?>
                <form>
                    <div class="form-group row">
                        <div class="col-sm-12 mandatory">Anrede</div>
                        <div class="col-sm-12">
                            <div class="custom-control custom-radio">
                                <input type="radio" id="inputFemale" name="salutation" class="custom-control-input" value="female">
                                <label class="custom-control-label" for="inputFemale">Frau</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input checked type="radio" id="inputMale" name="salutation" class="custom-control-input" value="male">
                                <label class="custom-control-label" for="inputMale">Herr</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputTitle">Titel</label>
                            <input type="text" class="form-control" id="inputTitle"  name="title" value="Ing.">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="mandatory" for="inputFirstname">Vorname</label>
                        <input type="text" class="form-control " id="inputFirstname" name="firstname" value="Markus">
                    </div>
                    <div class="form-group">
                        <label class="mandatory" for="inputLastname">Nachname</label>
                        <input type="text" class="form-control " id="inputLastname"  name="lastname" value="Brunner">
                    </div>
                    <div class="form-group"> <?php // @ToDo ?>
                        <label class="mandatory" for="inputMail">E-Mailadresse</label>
                        <input type="text" class="form-control" id="inputMail" name="mail" value="mb@mb.at">
                    </div>
                    <div class="form-row"> <?php // @ToDo ?>
                        <div class="form-group col-md-6">
                            <label for="inputCity">Ort</label>
                            <input type="text" class="form-control" id="inputCity" name="city" value="Krems">
                        </div>
                        <div class="form-group col-md-4"> <?php // @ToDo ?>
                            <label for="inputStreet">Straße</label>
                            <input type="text" class="form-control" id="inputStreet" name="street" value="Alauntal">
                        </div>
                        <div class="form-group col-md-2"> <?php // @ToDo ?>
                            <label for="inputZip">Plz</label>
                            <input type="text" class="form-control" id="inputZip" name="zip" value="3500">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="mandatory" for="inputState">Bundesland</label>
                            <select id="inputState" class="form-control" name="state">
                                <option>NÖ</option>
                                <option>Wien</option>
                                <option>OÖ</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-sm-12">
                            <label class="mandatory" for="">Mich interessiert</label>
                            <div class="custom-control custom-checkbox">
                                <input checked id="sew" class="custom-control-input" type="checkbox" name="interests[]"
                                       value="Softwareentwicklung"><label
                                        for="sew" class="custom-control-label">Softwareentwicklung</label></div>
                            <div class="custom-control custom-checkbox">
                                <input checked id="medt" class="custom-control-input" type="checkbox" name="interests[]"
                                       value="Medientechnik"><label id="medt"
                                                                    class="custom-control-label">Medientechnik</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input id="syt" class="custom-control-input" type="checkbox" name="interests[]"
                                       value="Systemtechnik"><label for="syt"
                                                                    class="custom-control-label">Systemtechnik</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="sBtn" class="btn btn-primary">Abschicken</button>
                </form>
        </div>
    </div>
</div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>

<!-- Option 2: jQuery, Popper.js, and Bootstrap JS
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
-->
</body>
</html>