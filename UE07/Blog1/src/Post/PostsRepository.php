<?php

namespace App\Post;
use PDO;

class PostsRepository
{
  private $pdo;

  /**
   * PostsRepository constructor.
   * @param PDO $pdo
   */
  public function __construct(PDO $pdo)
  {
    $this->pdo = $pdo;
  }

  /**
   * @return array
   */
  function fetchPosts(): array
  {
    $posts=$this->pdo->query("SELECT * FROM `posts`");
    $items=[];
    foreach ($posts as $post){
      array_push($items, new PostModel($post["id"], $post["title"], $post["content"]));
    }
    return $items;
  }

  /**
   * @param $id
   * @return PostModel
   */
  function fetchPost($id): PostModel
  {
    $stmt = $this->pdo->prepare("SELECT * FROM `posts` WHERE id = :id");
    $stmt->execute(['id' => $id]);
    $postArray = $stmt->fetch();
    return new PostModel($postArray["id"], $postArray["title"], $postArray["content"]);
  }
}

?>
