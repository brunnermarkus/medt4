<?php

namespace App\Post;

class PostModel
{
    public $id;
    public $title;
    public $content;

    /**
     * PostModel constructor.
     * @param $id
     * @param $title
     * @param $content
     */
    public function __construct($id, $title, $content)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
    }


}
 ?>
