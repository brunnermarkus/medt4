<?php require ("../init.php"); ?>

<?php include("inc/header.php"); ?>

<h1>Das ist die Startseite des Blogs.</h1>

<?php
  $postsRepsitory = new \App\Post\PostsRepository($pdo);
  $posts = $postsRepsitory->fetchPosts();
?>

<ul>
  <?php foreach ($posts as $post){ ?>
    <li>
      <a href="post.php?id=<?php echo $post->id; ?>">
        <?php echo $post->title; ?>
      </a>
    </li>
  <?php } ?>
</ul>

<?php include("inc/footer.php"); ?>
